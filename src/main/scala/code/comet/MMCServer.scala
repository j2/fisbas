package code.comet

import net.liftweb.actor.LiftActor
import net.liftweb.http.ListenerManager

/**
 * Created with IntelliJ IDEA.
 * User: j2
 * Date: 09-12-13
 * Time: 11:26 PM
 * To change this template use File | Settings | File Templates.
 */
class MMCServer extends LiftActor with ListenerManager {

  private var data: List[(Double, Double)] = Nil

  private var isLineal: Boolean = true

  override def lowPriority = {
    case DataServerMsg(x, y) =>
      data ::= (x, y)
      updateListeners()
    case _ =>
  }

  def createUpdate = DataServerUpdate(data)


}

case class DataServerMsg(x: Double, y: Double)

case class DataServerUpdate(data: List[(Double, Double)])
