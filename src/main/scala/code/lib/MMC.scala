package code.lib

/**
 * Created with IntelliJ IDEA.
 * User: j2
 * Date: 08-12-13
 * Time: 09:08 PM
 * To change this template use File | Settings | File Templates.
 */
case class MMC(data: List[(Double, Double)]) {

  def n = data.size

  def x = data.map(_._1).sum

  def x2 = data.map(s => math.pow(s._1, 2)).sum

  def y = data.map(_._2).sum

  def y2 = data.map(s => math.pow(s._2, 2)).sum

  def xy = data.map(s => s._1 * s._2).sum

  def A = (y * x2 - xy * x) / (n * x2 - math.pow(x, 2))

  def B = (n * xy - x * y ) / (n * x2 - math.pow(x, 2))

  def lambda = n * x2 - math.pow(x, 2)

  def d2 = y2 - 2 * A * y - 2 * B * xy + n * math.pow(A, 2) + 2 * A * B * x + math.pow(B, 2) * x2

  def o2 = d2 / (n - 2)

  def oA = math.sqrt(o2 * x2 / lambda)

  def oB = math.sqrt(o2 * n / lambda)

  def covXY = (n * xy - x * y) / math.pow(n, 2)

  def varX = x2 / n - math.pow(x / n, 2)

  def varY = y2 / n - math.pow(y / n, 2)

  def r = (n * xy - x * y) / math.sqrt((n * x2 - math.pow(x, 2)) * (n * y2 - math.pow(y, 2)))

  def b = B

  def a = math.pow(math.E, A)

  def ob = oB

  def oa = a * oA

}

object MMC {
  def apply(data: List[(Double, Double)], isLineal: Boolean = true): MMC = isLineal match {
    case false => new MMC(data filter (s => notZero(s._1) && notZero(s._2)) map(s => math.log(s._1) -> math.log(s._2)))
    case true => new MMC(data)
  }

  def isZero(x: Double) = x == 0.0 || x == 0.00 || x == 0.000 || x == 0.0000

  def notZero(x: Double) = !isZero(x)
}
