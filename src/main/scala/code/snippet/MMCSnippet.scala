package code.snippet

import net.liftmodules.widgets.flot.{FlotGridOptions, FlotOptions, Flot, FlotSerie}
import xml.NodeSeq
import net.liftweb.util.Helpers._
import net.liftweb.http.{RequestVar, SHtml}
import net.liftweb.http.js.JsCmds.{Run, Focus, SetHtml, Noop}
import net.liftweb.util.Helpers
import code.lib.MMC
import net.liftweb.common.Full
import net.liftweb.http.js.JE.{JsObj, JsTrue}

/**
 * Created with IntelliJ IDEA.
 * User: j2
 * Date: 08-12-13
 * Time: 10:49 PM
 * To change this template use File | Settings | File Templates.
 */

object data extends RequestVar[List[(Double, Double)]](Nil)
object isLineal extends RequestVar[Boolean](true)

object MMCSnippet {

  def flot = {
    val data_values: List[(Double,Double)] = isLineal.is match {
      case true => data.is
      case false => data.is.filter(s => MMC.notZero(s._1) && MMC.notZero(s._2)).map(s => math.log(s._1) -> math.log(s._2))
    }

    val data_to_plot = new FlotSerie() {
      override val data = data_values
    }

    Flot.renderJs("graph_area1", List(data_to_plot), new FlotOptions {
      override def series = Full(Map(
        "points" -> JsObj("show" -> true),
        "lines" -> JsObj("show" -> true)
      ))
      override def grid = Full(new FlotGridOptions {
        override def hoverable = Full(true)
      })
    }, Flot.script(NodeSeq.Empty))

  }

  def render = {
    var x = 0.0
    var y = 0.0
    "*" #> SHtml.idMemoize(body => {
      "#flot-headers" #> Flot.renderHead() &
      ".data-row" #> data.zipWithIndex.map(d => {
        ".n *" #> d._2 &
        ".x *" #> d._1._1 &
        ".y *" #> d._1._2
      }) &
      "@x" #> SHtml.ajaxText(x.toString, s => {
        x = Helpers.asDouble(s) openOr x
        Noop
      }, "id" -> "x") &
      "@y" #> SHtml.ajaxText(y.toString, s => {
        y = Helpers.asDouble(s) openOr x
        Noop
      }) &
      "#add [onclick]" #> SHtml.ajaxInvoke(() => {
        data.set(data.is ++ List(x -> y))
        x = 0.0
        y = 0.0
        body.setHtml() &
        Focus("x")
      }) &
      "type=checkbox" #> SHtml.ajaxCheckbox(isLineal.is, s => {
        isLineal.set(s)
        Noop
      }) &
      "#process" #> SHtml.ajaxButton("Procesar", () => {
        flot &
        plotHover &
        SetHtml("results", isLineal.is match {
          case true => calc.apply(lineal)
          case false => calc.apply(noLineal)
        })
        //body.setHtml()
      })
    })
  }

  def calc = {
    val mmc = if (isLineal.is) MMC(data.is) else MMC(data.is, false)
    "#sum-x *" #> mmc.x &
    "#sum-y *" #> mmc.y &
    "#sum-xy *" #> mmc.xy &
    "#sum-x-2 *" #> mmc.x2 &
    "#sum-y-2 *" #> mmc.y2 &
    "#sum-d-2 *" #> mmc.d2 &
    "#a1 *" #> mmc.A &
    "#b1 *" #> mmc.B &
    "#lambda *" #> mmc.lambda &
    "#n *" #> mmc.n &
    "#sigma-a1 *" #> mmc.oA &
    "#sigma-b1 *" #> mmc.oB &
    "#sigma-2 *" #> mmc.o2 &
    "#a2 *" #> mmc.a &
    "#b2 *" #> mmc.b &
    "#sigma-a2 *" #> mmc.oa &
    "#sigma-b2 *" #> mmc.ob &
    "#r *" #> mmc.r
  }

  def lineal = {
    <table class="table table-striped table-bordered table-hover table-condensed">
      <thead>
      <tr><th>Nombre</th><th>Valor</th></tr>
      </thead>
      <tbody>
      <tr><td>&sum;x </td><td id="sum-x"></td></tr>
      <tr><td>&sum;y </td><td id="sum-y"></td></tr>
      <tr><td>&sum;xy </td><td id="sum-xy"></td></tr>
      <tr><td>&sum;x<sup>2</sup> </td><td id="sum-x-2"></td></tr>
      <tr><td>&sum;y<sup>2</sup> </td><td id="sum-y-2"></td></tr>
      <tr><td>&sum;d<sup>2</sup><sub>i</sub> </td><td id="sum-d-2"></td></tr>
      <tr><td>A </td><td id="a1"></td></tr>
      <tr><td>B </td><td id="b1"></td></tr>
      <tr><td>&Lambda; </td><td id="lambda"></td></tr>
      <tr><td>n </td><td id="n"></td></tr>
      <tr><td>&sigma;<sub>A</sub> </td><td id="sigma-a1"></td></tr>
      <tr><td>&sigma;<sub>B</sub> </td><td id="sigma-b1"></td></tr>
      <tr><td>&sigma;<sup>2</sup> </td><td id="sigma-2"></td></tr>
      <tr><td>r </td><td id="r"></td></tr>
      </tbody>
    </table>
  }

  def noLineal = {
    <table class="table table-striped table-bordered table-hover table-condensed">
      <thead>
      <tr><th>Nombre</th><th>Valor</th></tr>
      </thead>
      <tbody>
      <tr><td>&sum;x </td><td id="sum-x"></td></tr>
      <tr><td>&sum;y </td><td id="sum-y"></td></tr>
      <tr><td>&sum;xy </td><td id="sum-xy"></td></tr>
      <tr><td>&sum;x<sup>2</sup> </td><td id="sum-x-2"></td></tr>
      <tr><td>&sum;y<sup>2</sup> </td><td id="sum-y-2"></td></tr>
      <tr><td>&sum;d<sup>2</sup><sub>i</sub> </td><td id="sum-d-2"></td></tr>
      <tr><td>A </td><td id="a1"></td></tr>
      <tr><td>B </td><td id="b1"></td></tr>
      <tr><td>&Lambda; </td><td id="lambda"></td></tr>
      <tr><td>n </td><td id="n"></td></tr>
      <tr><td>&sigma;<sub>A</sub> </td><td id="sigma-a1"></td></tr>
      <tr><td>&sigma;<sub>B</sub> </td><td id="sigma-b1"></td></tr>
      <tr><td>&sigma;<sup>2</sup> </td><td id="sigma-2"></td></tr>
      <tr><td>a </td><td id="a2"></td></tr>
      <tr><td>b </td><td id="b2"></td></tr>
      <tr><td>&sigma;<sub>a</sub> </td><td id="sigma-a2"></td></tr>
      <tr><td>&sigma;<sub>b</sub> </td><td id="sigma-b2"></td></tr>
      <tr><td>r </td><td id="r"></td></tr>
      </tbody>
    </table>
  }

  def plotHover = {
    Run("""$("<div id='tooltip'></div>").css({
      position: "absolute",
      display: "none",
      border: "1px solid #fdd",
      padding: "2px",
      "background-color": "#fee",
      opacity: 0.80
    }).appendTo("body");

    $("#graph_area1").bind("plothover", function (event, pos, item) {

      var str = "(" + pos.x.toFixed(2) + ", " + pos.y.toFixed(2) + ")";
      $("#hoverdata").text(str);

      if (item) {
        var x = item.datapoint[0].toFixed(2),
        y = item.datapoint[1].toFixed(2);

        $("#tooltip").html("x = " + x + "," + "y = " + y)
          .css({top: item.pageY+5, left: item.pageX+5})
          .fadeIn(200);
      } else {
        $("#tooltip").hide();
      }
    });""")
  }
}
